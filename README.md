# Jupyter Notebook (bash) - Workflow initiation

This Notebook introduces two workflow engine: Snakemake and Nextflow.

It explores the dependency management and the execution locally and on a HPC cluster (SLURM)

It will involve the two workflow git repositories:
 - [smk_preprocessing](https://gitlab.com/ifb-elixirfr/training/workflows/smk_preprocessing)
 - [nf_preprocessing](https://gitlab.com/ifb-elixirfr/training/workflows/nf_preprocessing)

⏱️ Duration : 3h

📑 Outlines:

- Snakemake
  - Explore a smk workflow
  - Run a smk workflow
    - Locally
    - On a HPC cluster (SLURM)
  - Rerun
  - Manage the dependency
    - Singularity (Apptainer)
    - Conda
- Nextflow
  - Explore a NF workflow
  - Run a NF workflow
    - Locally
    - On a HPC cluster (SLURM)
  - Rerun
  - Manage the dependency
    - Singularity (Apptainer)
    - Conda
  - Hands-on
    - Add a new step: fastp

## Prerequisites

### Computing resources
#### On the local computer
 
 - 1 CPU
 - 2 GB of RAM

#### On the HPC infrastructure
 
 - 6 CPU (or less)
 - 2 GB of RAM

### Software

 - Snakemake
 - Nextflow

Optional
 - Apptainer
 - Env module
 - Docker (for execution on a personnal computer)

